"use strict";
// 1. Функции в програмирование нужны для того, чтоб не дублировать код,
// чтоб использовать написаный код в разных местах (переиспользовать), и сделать
// код более структурированым.
// 2. Передавать аргументы в функцию нужно для того, чтоб функция выполняла
// разные действия, и была динамичной.
// 3. Оператор return заканчивает действиt функции, все что мы пишем после return
// закидывается в значение, которое функция возвращает после своего действия.

// Первый способ:
// function calc() {
//     let num1;
//     do {
//         num1 = +prompt("Введите первое число")
//     } while (!Number.isInteger(num1));
//     let num2;
//     do {
//         num2 = +prompt("Введите второе число")
//     } while (!Number.isInteger(num2));
//     const symbol = prompt("Введите действие(+, -, *, /)");
//     if (symbol === "+") {
//     return num1 + num2;
//     } else if (symbol === "-") {
//     return num1 - num2;
//     } else if (symbol === "*") {
//       return num1 * num2;
//     } else if (symbol === "/") {
//     return num1 / num2;
//     }
// }
// console.log("Результат:",calc());

// Второй способ:
let num1 = +prompt("Введите первое число");
let num2 = +prompt("Введите второе число");
while (!num1 || !num2) {
    num1 = +prompt('Введите первое число');
    num2 = +prompt('Введите второе число');
}

const symbol = prompt("Введите действие(+, -, *, /)");

function calc () {  
    switch (symbol) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            return num1 / num2;
    }
};
console.log("Результат:",calc(num1, num2, symbol));

// Третий способ:
// const num1 = +prompt("Введите первое число");
// const num2 = +prompt("Введите второе число");
// const symbol = prompt("Введите действие(+, -, *, /)");

// function calc() {
//     let result = symbol === "+" ? num1 + num2 : symbol === "-" ? num1 - num2 : symbol === "*" ? num1 * num2 : symbol === "/" ? num1 / num2 : false;
//     return result;
// }
// console.log("Результат:",calc());




